package com.sms.controller;


import com.sms.dao.OrganizationUserDao;
import com.sms.dao.UserDao;
import com.sms.dao.UserRolesDao;
import com.sms.model.OrganizationUsers;
import com.sms.model.User;
import com.sms.model.UserRoles;
import com.sms.util.AdvancedEncryptionStandard;
import com.sms.util.JsonMessage;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping(value="/api/v1")
@Api(value="AdminOperations", description="Operations pertaining to products in Admin")
public class AdminController {
private static Logger log = LoggerFactory.getLogger(AdminController.class);
  @Autowired
  private OrganizationUserDao _organizationUserDao;


  @RequestMapping(value="/createAdmin",method = RequestMethod.POST)
  @ApiOperation( "Create Admin")
  public @ResponseBody JsonMessage createAdmin(@RequestBody final  OrganizationUsers user) 
  {
	JsonMessage response=new JsonMessage();
    try 
    {
    	if((_organizationUserDao.getByEmail(user.getEmail(),user.getOrgId())).size()>0)
    	{
    		response.setMessage("Email already exists.");
       	  	response.setStatuscode(208);
       	  	return response;
    	}else
    	{
		user.setPassword(AdvancedEncryptionStandard.encrypt(user.getPassword()));
		user.setCreateDate(new Date());
		user.setUpdateDate(new Date());
		user.setUserType(2);;
		user.setStatus("Y");
		int id=_organizationUserDao.save(user);
		log.info("Admin Created Successfully"+new Date());
		RestTemplate restTemplate = new RestTemplate();
		Map<String,Object> data1=new HashMap<String,Object>();
		 data1.put("from","SMSAPP<bhanu735@gmail.com>");
		 data1.put("to",user.getEmail());
		 data1.put("subject","Account Conformation");
		// data1.put("text","Hi <b>"+user.getFullName()+"</b><br><br>Congratulations! Your account has been created.Please click on the link to verify your email address and start using Engage.<br><br><a href='"+portalURL+"/userconfirmation.html?keyconfirm="+AdvancedEncryptionStandard.encrypt(user.getEmail())+"'>Verify</a><br><br>Thank You,<br>Team Engage at Quantified Care");
		 data1.put("status",true);
		//restTemplate.postForObject("http://35.166.195.23:8080/EmailMicroservice/email/send", data1,String.class );
		response.setMessage("User registred successfully");
		response.setStatuscode(200);
		return response;
    	}
    }
    catch(Exception ex) 
    {
    	log.error(ex.getMessage());
    	  response.setMessage(ex.getMessage());
    	  response.setStatuscode(203);
    	  return response;
    }
  }
  
  @RequestMapping(value="/{orgId}/getAllAdmins",method = RequestMethod.GET)
  @ApiOperation( "Get All Admins")
  public @ResponseBody JsonMessage getAllAdmins(@PathVariable("orgId")int orgId) 
  {
	JsonMessage response=new JsonMessage();
	List<OrganizationUsers> organizationUsers=new ArrayList<OrganizationUsers>();
    try 
    {
    	organizationUsers=_organizationUserDao.getAll(orgId);
		response.setMessage("List of admins");
		response.setData(organizationUsers);
   	  	response.setStatuscode(200);

		return response;
    	
    }
    catch(Exception ex) 
    {
    	  response.setMessage(ex.getMessage());
    	  response.setStatuscode(203);
    	  return response;
    }
  }
  @RequestMapping(value="/{orgId}/getAdmin/{emailId}",method = RequestMethod.GET)
  @ApiOperation( "Get Admin by Email")
  public @ResponseBody JsonMessage getAdminById(@PathVariable("emailId") String emailId,@PathVariable("orgId") int orgId) 
  {
	JsonMessage response=new JsonMessage();
	List<OrganizationUsers> organizationUsers=new ArrayList<OrganizationUsers>();
	organizationUsers=_organizationUserDao.getAll(orgId);
    try 
    {
    		response.setMessage("Admin info");
    		response.setData(organizationUsers);
       	  	response.setStatuscode(200);
       	  	return response;	
    }
    catch(Exception ex) 
    {
    	  response.setMessage(ex.getMessage());
    	  response.setStatuscode(203);
    	  return response;
    }
  }
} // class UserController