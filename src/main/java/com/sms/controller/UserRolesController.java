package com.sms.controller;

import com.sms.dao.UserDao;
import com.sms.dao.UserRolesDao;
import com.sms.model.*;

import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/UserRoles")
public class UserRolesController {

  @Autowired
  private UserRolesDao _userRolesDao;
  
  @RequestMapping(value="/delete")
  @ResponseBody
  public String delete(Integer id) {
    try {
      UserRoles user = new UserRoles(id);
      _userRolesDao.delete(user);
    }
    catch(Exception ex) {
      return ex.getMessage();
    }
    return "User succesfully deleted!";
  }
  @RequestMapping(value="/get-by-userroles" ,method=RequestMethod.GET)
  public @ResponseBody List getAllUserRoles()
  {
	  List lst =(List)_userRolesDao.getAll();
	  return lst;
  }
  
//  @RequestMapping(value="/get-by-email")
//  
//  public String getByEmail(String email) {
//    String userId;
//    try {
//      User user = _userDao.getByEmail(email);
//      userId = String.valueOf(user.getId());
//    }
//    catch(Exception ex) {
//      return "User not found";
//    }
//    return "The user id is: " + userId;
//  }

  @RequestMapping(value="/save",method = RequestMethod.POST)

  public ResponseEntity create(@RequestBody final UserRoles user) {
    try {
      //User user = new User(email, name);
    	user.setCreatedat(new Date());
    	_userRolesDao.save(user);
    }
    catch(Exception ex) {
      return new ResponseEntity(ex.getMessage(), HttpStatus.OK);
    }
    return new ResponseEntity("User roles saved successfully", HttpStatus.OK);
  }

} // class UserController