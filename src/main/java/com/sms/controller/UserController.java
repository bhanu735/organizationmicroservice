package com.sms.controller;


import com.sms.dao.UserDao;
import com.sms.dao.UserRolesDao;
import com.sms.model.User;
import com.sms.model.UserRoles;
import com.sms.util.AdvancedEncryptionStandard;
import com.sms.util.JsonMessage;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping(value="/api/v1")
@Api(value="Userlist", description="Operations pertaining to products in Online Store")
public class UserController {
	  private static Logger log = LoggerFactory.getLogger(UserController.class);
  @Autowired
  private UserDao _userDao;
  @Autowired
  private UserRolesDao _userRolesDao; 


  @ApiOperation(value = "Add a product")
  @RequestMapping(value="/profile",method = RequestMethod.POST)
  public @ResponseBody JsonMessage getProfile(@RequestBody Map<String, String> json) 
  {
	  JsonMessage response=new JsonMessage();
	 try 
	  {
	  Map<String,Object> data=new HashMap<String,Object>();
	  
	  if(!(_userDao.getByEmail(json.get("emailid"))).isEmpty())
	  {
		  List<User> user =_userDao.getByEmail(json.get("emailid"));
		  data.put("userData", user);
		  response.setMessage("User profile");
		  response.setStatuscode(200);
		  response.setData(data);
		  return response;
	  }else
	  {
		  response.setMessage("No profile information found.");
		  response.setStatuscode(204);
		  return response;
	  }
	  }catch(Exception ex)
	 {
		  response.setMessage("No profile information found.");
		  response.setStatuscode(204);
		  return response;
	 }
  }

  //Change Password
  @RequestMapping(value="/change_password",method = RequestMethod.POST)
  public @ResponseBody JsonMessage changePassword(@RequestBody Map<String, String> json) 
  {
	  JsonMessage response=new JsonMessage();
	 try
	 {
		 if(!(_userDao.getByEmail(json.get("emailid"))).isEmpty())
		  {
			  User user=_userDao.getByUserName(json.get("emailid"),AdvancedEncryptionStandard.decrypt(json.get("oldpassword")));
			  user.setPassword(AdvancedEncryptionStandard.encrypt(json.get("password")));
			  _userDao.update(user);
			  response.setMessage("Password updated successfully.");
			  response.setStatuscode(200);
			  return response;
		  }else
		  {
			  response.setMessage("Email not registered.");
			  response.setStatuscode(204);
			  return response;
		  }
	 }catch (Exception e) {
		 response.setMessage("Email not registered.");
		  response.setStatuscode(204);
		  return response;
	}
	 	 
  }

} // class UserController