package com.sms.controller;


import java.util.Date;
import java.util.HashMap;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.sms.dao.OrganizationUserDao;
import com.sms.dao.UserDao;
import com.sms.dao.UserRolesDao;
import com.sms.model.OrganizationUsers;
import com.sms.model.User;
import com.sms.util.AdvancedEncryptionStandard;
import com.sms.util.JsonMessage;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@RestController
public class LoginController {

@Value("${microService.URL}")
public String baseURL;
@Value("${portal.URL}")
public String portalURL;
  @Autowired
  private UserDao _userDao;
  @Autowired
  private OrganizationUserDao _organizationUserDao;

  
  //Login User MicroService
  @RequestMapping(value="/login",method = RequestMethod.POST)

  public @ResponseBody JsonMessage login(@RequestBody final User user) {
	  JsonMessage response=new JsonMessage();
    try {
    	
     User validateUser= _userDao.getByUserName(user.getEmail(),AdvancedEncryptionStandard.encrypt(user.getPassword()));
      if(!validateUser.getEmail().equalsIgnoreCase(null))
      { 
    	if(validateUser.getStatus()=="N" )
    	{
    		response.setMessage("Account is disbled.");
    		response.setStatuscode(401);
    		 return response ;
    	}
    	 Map<String,Object> data1=new HashMap<String,Object>();
    	 data1.put("UserBacsicInfo", validateUser);
    	// data1.put("UserDetailsInfo", userdetails);
    	 Date today=new Date();
    	 long ltime=today.getTime()+1*24*60*60*1000;
    	 Date expDate=new Date(ltime);
    	 String token= Jwts.builder().setSubject(validateUser.getEmail()).setIssuedAt(new Date()).setExpiration(expDate)
    			 .signWith(SignatureAlgorithm.HS256, "secretkey").compact();
    	data1.put("token",  token);
    	  response.setMessage("User login successfully.");
    	  response.setData(data1);
    	 // response.setData(userdetails);
    	  response.setStatuscode(200);
    	  return response ;
      }else
      {
    	  response.setMessage("Incorrect Email/Pasword combination. Please try again.");
    	  response.setStatuscode(204);
      return response;
      }
    }
    catch(Exception ex) {
    	response.setMessage(ex.getMessage());
    	  //response.setMessage("Incorrect Email/Pasword combination. Please try again.");
    	  response.setStatuscode(204);
      return response;
    }
  }
  
  @RequestMapping(value="/orgLogin",method = RequestMethod.POST)
  public @ResponseBody JsonMessage orgLogin(@RequestBody final OrganizationUsers user) {
	  
	  JsonMessage response=new JsonMessage();
    try {
    	
    	OrganizationUsers validateUser= _organizationUserDao.getByUserName(user.getEmail(),AdvancedEncryptionStandard.encrypt(user.getPassword()),user.getOrgId());
    	System.out.println(user.getEmail());
      if(!validateUser.getEmail().equalsIgnoreCase(null))
      { 
    	if(validateUser.getStatus()=="N" )
    	{
    		response.setMessage("Account is disbled.");
    		response.setStatuscode(200);
    		 return response ;
    	}
    	 Map<String,Object> data1=new HashMap<String,Object>();
    	 data1.put("UserBacsicInfo", validateUser);
    	// data1.put("UserDetailsInfo", userdetails);
    	 Date today=new Date();
    	 long ltime=today.getTime()+1*24*60*60*1000;
    	 Date expDate=new Date(ltime);
    	 String token= Jwts.builder().setSubject(validateUser.getEmail()).setIssuedAt(new Date()).setExpiration(expDate)
    			 .signWith(SignatureAlgorithm.HS256, "secretkey").compact();
    	 data1.put("token",  token);
    	 response.setMessage("User login successfully.");
    	 response.setData(data1);
    	 
    	  response.setStatuscode(200);
    	  return response ;
      }else
      {
    	  response.setMessage("Incorrect Email/Pasword combination. Please try again.");
    	  response.setStatuscode(204);
      return response;
      }
    }
    catch(Exception ex) {
    	response.setMessage(ex.getMessage());
    	  //response.setMessage("Incorrect Email/Pasword combination. Please try again.");
    	  response.setStatuscode(204);
      return response;
    }
  }
  
  //Validate Email
  @RequestMapping(value="/IsEmailExists",method = RequestMethod.POST)
  public @ResponseBody JsonMessage IsEmailExists(@RequestBody Map<String, String> json) 
  {
	  JsonMessage response=new JsonMessage();
	 try 
	  {
		 if((_userDao.getByEmail(json.get("emailid")).size()>0))
	  {
		  response.setMessage("Email Already Exists");
		  response.setStatuscode(200);
		  return response;
	  }else
	  {
		  response.setMessage("No profile information found.");
		  response.setStatuscode(204);
		  return response;
	  }
	  }catch(Exception ex)
	 {
		  response.setMessage("No profile information found.");
		  response.setStatuscode(204);
		  return response;
	 }
  }
//Verify Email Id
  @RequestMapping(value="/verify_email",method = RequestMethod.POST)
  public @ResponseBody JsonMessage verifyEmail(@RequestBody Map<String, String> json) 
  {
	  JsonMessage response=new JsonMessage();
	  try 
	  {
	if(!(_userDao.getByEmail(AdvancedEncryptionStandard.decrypt(json.get("emailid"))).size()>0))
	  {
		  response.setMessage("Invalid account.");
		  response.setStatuscode(204);
		  return response;
	  }else
	  {
		  User user=_userDao.getById(AdvancedEncryptionStandard.decrypt(json.get("emailid")));
		  user.setStatus("Y");
		  _userDao.update(user);
		  response.setMessage("Account activated successfully.");
		  response.setStatuscode(200);
		  return response;
	  }
	  }catch (Exception e) {
		  response.setMessage(e.getMessage());
		  response.setStatuscode(203);
		  return response;
	}
  }
//Re-SetPassword
  @RequestMapping(value="/verify_emailForgetpwd",method = RequestMethod.POST)
  public @ResponseBody JsonMessage resetPassword(@RequestBody Map<String, String> json) 
  {
	  JsonMessage response=new JsonMessage();
	 try
	 {
		 if((_userDao.getByEmail(AdvancedEncryptionStandard.decrypt(json.get("emailid"))).size()>0))
		  {
			 User user=_userDao.getById(AdvancedEncryptionStandard.decrypt(json.get("emailid")));
			  user.setPassword(AdvancedEncryptionStandard.encrypt(json.get("password")));
			  _userDao.update(user);
			  response.setMessage("Password updated successfully.");
			  response.setStatuscode(200);
			  return response;
		  }else
		  {
			  response.setMessage("Email not registered.");
			  response.setStatuscode(204);
			  return response;
		  }
	 }catch (Exception e) {
		 response.setMessage(e.getMessage());
		  response.setStatuscode(203);
		  return response;
	}
	 	 
  }
  
//Forget password
  @RequestMapping(value="/forget_password",method = RequestMethod.POST)
  public @ResponseBody JsonMessage forgetPassword(@RequestBody Map<String, String> json) 
  {
	  JsonMessage response=new JsonMessage();
	 try{
		 if((_userDao.getByEmail(json.get("emailid")).size()>0))
		  {
			 User user=_userDao.getById(json.get("emailid"));
			  response.setMessage("Password sent to your email.");
			  response.setStatuscode(200);
			  RestTemplate restTemplate = new RestTemplate();
				Map<String,Object> data=new HashMap<String,Object>();
				data.put("from","EngageApp<bhanu735@gmail.com>");
				data.put("to",user.getEmail());
				data.put("subject","Forget Password");
				data.put("text","Dear <b>"+user.getFullName()+"</b><br><br>Please click below link for reset the password.<br><br><a href='"+portalURL+"/forgotkeyconfirm.html?keyconfirm="+AdvancedEncryptionStandard.encrypt(user.getEmail())+"'>Click</a><br><br>Regards,<br>Team Engage.");
				data.put("status",true);
			  restTemplate.postForObject("http://35.166.195.23:8080/EmailMicroservice/email/send", data,String.class );
			  return response;
		  }else
		  {
			  response.setMessage("Email not registered.");
			  response.setStatuscode(204);
			  return response;
		  }
	 }catch (Exception e) {
		 response.setMessage(e.getMessage());
		  response.setStatuscode(204);
		  return response;
	}	  
  }

} // class UserController