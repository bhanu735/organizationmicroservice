package com.sms.controller;


import com.sms.dao.GroupsDao;
import com.sms.dao.SubjectsDao;
import com.sms.dao.UserDao;
import com.sms.dao.UserRolesDao;
import com.sms.model.Groups;
import com.sms.model.Organizations;
import com.sms.model.Subjects;
import com.sms.model.User;
import com.sms.model.UserRoles;
import com.sms.util.AdvancedEncryptionStandard;
import com.sms.util.JsonMessage;

import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping(value="/api/v1")
public class SubjectsController {
	  private static Logger log = LoggerFactory.getLogger(SubjectsController.class);

  @Autowired
  private SubjectsDao _subjectsDao; 


  @RequestMapping(value="/getSubjectsInfo/{groupname}",method = RequestMethod.GET)
  public @ResponseBody JsonMessage getProfile(@PathVariable("groupname") String groupname) 
  {
	  JsonMessage response=new JsonMessage();
	 try 
	  {
	  Map<String,Object> data=new HashMap<String,Object>();
	  List<Subjects> groups=_subjectsDao.getSubject(groupname);
	  if(groups!=null)
	  {
		  data.put("groupData", groups);
		  response.setMessage("Group Data");
		  response.setStatuscode(200);
		  response.setData(data);
		  return response;
	  }else
	  {
		  response.setMessage("No Subject information found.");
		  response.setStatuscode(204);
		  return response;
	  }
	  }catch(Exception ex)
	 {
		  response.setMessage("No Group information found.");
		  response.setStatuscode(204);
		  return response;
	 }
  }
  @RequestMapping(value="/createSubject",method = RequestMethod.POST)
  public @ResponseBody JsonMessage addSubject(@RequestBody final Subjects subject) 
  { 
	  JsonMessage response=new JsonMessage();
	  if(_subjectsDao.getSubject(subject.getName()).isEmpty())
	  {	
		  subject.setCreateDate(new Date());
		  subject.setUpdateDate(new Date());
		  subject.setStatus(true);
		  _subjectsDao.save(subject);
		  //Send email notification here
		  
		  response.setMessage("Subject created successfully");
		  response.setStatuscode(200);
		  return response;
		  
	  }else
	  {
		  response.setMessage("Subject already registered");
		  response.setStatuscode(208);
		  return response;
		  
	  }

  }

} 