package com.sms.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.sms.dao.OrganizationDao;
import com.sms.dao.OrganizationUserDao;
import com.sms.dao.UserDao;
import com.sms.dao.UserRolesDao;
import com.sms.model.OrganizationUsers;
import com.sms.model.Organizations;
import com.sms.model.User;
import com.sms.util.AdvancedEncryptionStandard;
import com.sms.util.JsonMessage;

import io.swagger.annotations.Api;

@RestController

public class OrganizationController {
	  private static Logger log = LoggerFactory.getLogger(UserController.class);
	  @Autowired
	  private OrganizationDao _organizationDao;
	  @Autowired
	  private OrganizationUserDao _organizationUserDao;

	 //Register Organization 
	  @RequestMapping(value="/createOrganization",method = RequestMethod.POST)
	  public @ResponseBody JsonMessage addOrganization(@RequestBody final Organizations organizations) 
	  { 
		  JsonMessage response=new JsonMessage();
		  if((_organizationDao.getByEmail(organizations.getEmail())).isEmpty())
		  {	
			  organizations.setCreateDate(new Date());
			  organizations.setUpdateDate(new Date());
			  organizations.setStatus("N");
			  Integer id= _organizationDao.save(organizations);
			  OrganizationUsers orguser=new OrganizationUsers();
			  orguser.setEmail(organizations.getEmail());
			  orguser.setFullName(organizations.getSpocName());
			  orguser.setOrgId(id);
			  try {
				orguser.setPassword(AdvancedEncryptionStandard.encrypt(organizations.getPassword()));
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			  orguser.setPhone(organizations.getPhone());
			  orguser.setStatus("Y");
			  orguser.setUserType(2);
			  orguser.setCreateDate(new Date());
			  orguser.setUpdateDate(new Date());
			  
			  _organizationUserDao.save(orguser);
			  //Send email notification here
			  RestTemplate restTemplate = new RestTemplate();
				Map<String,Object> data1=new HashMap<String,Object>();
				 data1.put("from","EngageApp<bhanu735@gmail.com>");
				 data1.put("to",organizations.getEmail());
				 data1.put("subject","Account Conformation");
				 data1.put("status",true);
				 try {
					data1.put("text","Hi <b>"+organizations.getSpocName()+"</b><br><br>Congratulations! Your account has been created.Please click on the link to verify your email address and start using Engage.<br><br><a href='www.google.com/userconfirmation.html?keyconfirm="+AdvancedEncryptionStandard.encrypt(organizations.getEmail())+"'>Verify</a><br><br>Thank You,<br>Team EMS.");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				 
				 restTemplate.postForObject("http://localhost:8082/email/send", data1,String.class );
			  response.setMessage("Organization Created Successfully");
			  response.setStatuscode(200);
			  return response;
			  
		  }else
		  {
			  response.setMessage("Organization Already Registred");
			  response.setStatuscode(208);
			  return response;
			  
		  }
		  
		  
	  }
	  
	  
}
