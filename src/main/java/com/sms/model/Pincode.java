package com.sms.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="sms_picodes")
public class Pincode {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private int pinCode;
	private String state;
	private String district;
	private String city;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPinCode() {
		return pinCode;
	}
	public void setPinCode(int pinCode) {
		this.pinCode = pinCode;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public Pincode() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Pincode(int id, int pinCode, String state, String district, String city) {
		super();
		this.id = id;
		this.pinCode = pinCode;
		this.state = state;
		this.district = district;
		this.city = city;
	}
	

}
