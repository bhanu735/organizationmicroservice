package com.sms.model;

import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.sms.util.AdvancedEncryptionStandard;

@Entity
@Table(name="sms_organizationusers")
public class OrganizationUsers {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int id;
  private int orgId;
  @Column(name="full_name")
  private String fullName;
  private String email;
  private String password;
  private String phone;
  @Column(name="user_type")
  private int userType; //1-admin,2-staff,3-sdudent,5-root
  private String status;
  private Date createDate;
  private Date updateDate;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public int getOrgId() {
	return orgId;
}
public void setOrgId(int orgId) {
	this.orgId = orgId;
}
public String getFullName() {
	return fullName;
}
public void setFullName(String fullName) {
	this.fullName = fullName;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {

		this.password  = password;
	
}
public String getPhone() {
	return phone;
}
public void setPhone(String phone) {
	this.phone = phone;
}

public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public Date getCreateDate() {
	return createDate;
}
public void setCreateDate(Date createDate) {
	this.createDate = createDate;
}
public Date getUpdateDate() {
	return updateDate;
}
public void setUpdateDate(Date updateDate) {
	this.updateDate = updateDate;
}
public int getUserType() {
	return userType;
}
public void setUserType(int userType) {
	this.userType = userType;
}
public OrganizationUsers(int id, int orgId, String fullName, String email, String password, String phone, int userType,
		String status, Date createDate, Date updateDate) {
	super();
	this.id = id;
	this.orgId = orgId;
	this.fullName = fullName;
	this.email = email;
	this.password = password;
	this.phone = phone;
	this.userType = userType;
	this.status = status;
	this.createDate = createDate;
	this.updateDate = updateDate;
}
public OrganizationUsers() {
	super();
	// TODO Auto-generated constructor stub
}

  
  
}