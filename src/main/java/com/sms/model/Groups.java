package com.sms.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name="sms_groups")
public class Groups {
@Id
@GeneratedValue(strategy = GenerationType.AUTO)
private int id;
@Column(name="gname")
private String name;
private int assiginedBy;
private String description;
private boolean status;
private Date createDate;
private Date updateDate;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public int getAssiginedBy() {
	return assiginedBy;
}
public void setAssiginedBy(int assiginedBy) {
	this.assiginedBy = assiginedBy;
}
public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}
public boolean isStatus() {
	return status;
}
public void setStatus(boolean status) {
	this.status = status;
}
public Date getCreateDate() {
	return createDate;
}
public void setCreateDate(Date createDate) {
	this.createDate = createDate;
}
public Date getUpdateDate() {
	return updateDate;
}
public void setUpdateDate(Date updateDate) {
	this.updateDate = updateDate;
}
public Groups() {
	super();
	// TODO Auto-generated constructor stub
}
public Groups(int id, String name, int assiginedBy, String description, boolean status, Date createDate,
		Date updateDate) {
	super();
	this.id = id;
	this.name = name;
	this.assiginedBy = assiginedBy;
	this.description = description;
	this.status = status;
	this.createDate = createDate;
	this.updateDate = updateDate;
}



}
