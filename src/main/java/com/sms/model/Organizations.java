package com.sms.model;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="sms_organization")
public class Organizations {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int id;
  private String name;
  private String email;
  @Column(name="spocName")
  private String spocName;
  private String phone;
  private String address;
  private String website;
  private String pincode;
  private String password;
  private String status;
  private Date createDate;
  private Date updateDate;
  
  
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getSpocName() {
	return spocName;
}
public void setSpocName(String spocName) {
	this.spocName = spocName;
}
public String getPhone() {
	return phone;
}
public void setPhone(String phone) {
	this.phone = phone;
}
public String getAddress() {
	return address;
}
public void setAddress(String address) {
	this.address = address;
}
public String getWebsite() {
	return website;
}
public void setWebsite(String website) {
	this.website = website;
}
public String getPincode() {
	return pincode;
}
public void setPincode(String pincode) {
	this.pincode = pincode;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public Date getCreateDate() {
	return createDate;
}
public void setCreateDate(Date createDate) {
	this.createDate = createDate;
}
public Date getUpdateDate() {
	return updateDate;
}
public void setUpdateDate(Date updateDate) {
	this.updateDate = updateDate;
}
public Organizations(int id, String name, String email, String spocName, String phone, String address, String website,
		String pincode, String password, String status, Date createDate, Date updateDate) {
	super();
	this.id = id;
	this.name = name;
	this.email = email;
	this.spocName = spocName;
	this.phone = phone;
	this.address = address;
	this.website = website;
	this.pincode = pincode;
	this.password = password;
	this.status = status;
	this.createDate = createDate;
	this.updateDate = updateDate;
}
public Organizations() {
	super();
	// TODO Auto-generated constructor stub
}

  
} // class User