package com.sms.model;

import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.sms.util.AdvancedEncryptionStandard;

@Entity
@Table(name="sms_users")
public class User {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private int id;
  private int regNo;
  private int orgId;
  private int groupId;
  @Column(name="full_name")
  private String fullName;
  @Column(name="guardianName")
  private String guardianName;
  @Column(name="dob")
  private Date dob;
  @Column(name="pob")
  private String birthPlace;
  private String classStudied;
  private String caste;
  private String subCaste;
  private String identifications;
  private String email;
  private String password;
  private String phone;
  private String imgUrl;
  @Column(name="user_type")
  private String userType; //A-admin,P-staff,s-sdudent,r-root
  private String status;
  private Date createDate;
  private Date updateDate;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public int getRegNo() {
	return regNo;
}
public void setRegNo(int regNo) {
	this.regNo = regNo;
}
public int getOrgId() {
	return orgId;
}
public void setOrgId(int orgId) {
	this.orgId = orgId;
}
public int getGroupId() {
	return groupId;
}
public void setGroupId(int groupId) {
	this.groupId = groupId;
}
public String getFullName() {
	return fullName;
}
public void setFullName(String fullName) {
	this.fullName = fullName;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	try {
		this.password  = AdvancedEncryptionStandard.encrypt(password);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		this.password =null;
	}
}
public String getPhone() {
	return phone;
}
public void setPhone(String phone) {
	this.phone = phone;
}
public String getImgUrl() {
	return imgUrl;
}
public void setImgUrl(String imgUrl) {
	this.imgUrl = imgUrl;
}
public String getUserType() {
	return userType;
}
public void setUserType(String userType) {
	this.userType = userType;
}
public String getStatus() {
	return status;
}
public void setStatus(String status) {
	this.status = status;
}
public Date getCreateDate() {
	return createDate;
}
public void setCreateDate(Date createDate) {
	this.createDate = createDate;
}
public Date getUpdateDate() {
	return updateDate;
}

public void setUpdateDate(Date updateDate) {
	this.updateDate = updateDate;
}

public User() {
	super();
}
public String getGuardianName() {
	return guardianName;
}
public void setGuardianName(String guardianName) {
	this.guardianName = guardianName;
}
public Date getDob() {
	return dob;
}
public void setDob(Date dob) {
	this.dob = dob;
}
public String getBirthPlace() {
	return birthPlace;
}
public void setBirthPlace(String birthPlace) {
	this.birthPlace = birthPlace;
}
public String getClassStudied() {
	return classStudied;
}
public void setClassStudied(String classStudied) {
	this.classStudied = classStudied;
}
public String getCaste() {
	return caste;
}
public void setCaste(String caste) {
	this.caste = caste;
}
public String getSubCaste() {
	return subCaste;
}
public void setSubCaste(String subCaste) {
	this.subCaste = subCaste;
}
public String getIdentifications() {
	return identifications;
}
public void setIdentifications(String identifications) {
	this.identifications = identifications;
}
public User(int id, int regNo, int orgId, int groupId, String fullName, String guardianName, Date dob,
		String birthPlace, String classStudied, String caste, String subCaste, String identifications, String email,
		String password, String phone, String imgUrl, String userType, String status, Date createDate,
		Date updateDate) {
	super();
	this.id = id;
	this.regNo = regNo;
	this.orgId = orgId;
	this.groupId = groupId;
	this.fullName = fullName;
	this.guardianName = guardianName;
	this.dob = dob;
	this.birthPlace = birthPlace;
	this.classStudied = classStudied;
	this.caste = caste;
	this.subCaste = subCaste;
	this.identifications = identifications;
	this.email = email;
	this.password = password;
	this.phone = phone;
	this.imgUrl = imgUrl;
	this.userType = userType;
	this.status = status;
	this.createDate = createDate;
	this.updateDate = updateDate;
}


  
  
 
}