package com.sms.dao;

import java.math.BigInteger;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sms.model.User;

@Repository
@Transactional
public class UserDao {
  
  @Autowired
  private SessionFactory _sessionFactory;
  
  private Session getSession() {
    return _sessionFactory.getCurrentSession();
  }

  public int save(User user) {
    int id=(int) getSession().save(user);
    return id;
  }
  
  public void delete(User user) {
    getSession().delete(user);
    return;
  }
  
  @SuppressWarnings("unchecked")
  public List<User> getAll() {
    return getSession().createQuery("from User").list();
  }
  @SuppressWarnings("unchecked")
public List<String> getallPraticeNames() {
	  List<String> praticenames=null;
	  try
	  {
		  Criteria crit = getSession().createCriteria(User.class);
		  crit.setProjection(Projections.distinct(Projections.property("practiceName")));
		  praticenames=crit.list();
		  
		  return praticenames;
	  }catch(Exception ex)
	  {
		  return praticenames;
	  }
	  
  }
  @SuppressWarnings("unchecked")
public List<User> getByEmail(String email) {
	  List<User> user=null;
	  try
	  {
		  Criteria crit = getSession().createCriteria(User.class);
		  crit.add(Restrictions.eq("email",email));
		  user= crit.list();
		  
		  return user;
	  }catch(Exception ex)
	  {
		  return user;
	  }
    
  }

  public User getById(String email) {
	  User user=null;
	  try
	  {
		  
		  Criteria crit = getSession().createCriteria(User.class);
		  crit.add(Restrictions.eq("email",email));
		   user=(User) crit.uniqueResult();
	    return user;
	  }catch(Exception e)
	  {
		  System.out.println(e.getMessage());
		  return user;
	  }
	 
  }
  public User getByUserName(String user_name,String password) {
	  return (User) getSession().createQuery(
		        "from User where email = :email and password=:password")
		        .setParameter("email", user_name)
		        .setParameter("password", password)
		        .uniqueResult();
	  }

  public void update(User user) {
    getSession().update(user);
    return;
  }

} // class UserDao