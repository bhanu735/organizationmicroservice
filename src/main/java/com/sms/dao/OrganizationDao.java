package com.sms.dao;

import java.math.BigInteger;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sms.model.Organizations;
import com.sms.model.User;

@Repository
@Transactional
public class OrganizationDao {
  
  @Autowired
  private SessionFactory _sessionFactory;
  
  private Session getSession() {
    return _sessionFactory.getCurrentSession();
  }

  public Integer save(Organizations org) {
    Integer id= (Integer) getSession().save(org);
    return id;
  }
  
  public void delete(Organizations org) {
    getSession().delete(org);
    return;
  }
  
  @SuppressWarnings("unchecked")
  public List<Organizations> getAll() {
    return getSession().createQuery("from Organizations").list();
  }
  
  @SuppressWarnings("unchecked")
public List<Organizations> getByEmail(String email) {
	  List<Organizations> org=null;
	  try
	  {
		  Criteria crit = getSession().createCriteria(Organizations.class);
		  crit.add(Restrictions.eq("email",email));
		  org= crit.list();
		  
		  return org;
	  }catch(Exception ex)
	  {
		  return org;
	  }
    
  }

  public Organizations getById(String email) {
	  Organizations org=null;
	  try
	  {
		  
		  Criteria crit = getSession().createCriteria(Organizations.class);
		  crit.add(Restrictions.eq("email",email));
		   org=(Organizations) crit.uniqueResult();
	    return org;
	  }catch(Exception e)
	  {
		  System.out.println(e.getMessage());
		  return org;
	  }
	 
  }


  public void update(Organizations org) {
    getSession().update(org);
    return;
  }

} // class UserDao