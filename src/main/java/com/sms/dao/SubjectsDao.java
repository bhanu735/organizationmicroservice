package com.sms.dao;

import java.util.List;
import javax.transaction.Transactional;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.sms.model.Groups;
import com.sms.model.Organizations;
import com.sms.model.Subjects;


@Repository
@Transactional
public class SubjectsDao {
  
  @Autowired
  private SessionFactory _sessionFactory;
  
  private Session getSession() {
    return _sessionFactory.getCurrentSession();
  }

  public int save(Subjects groups) {
    int id=(int) getSession().save(groups);
    return id;
  }
  
  public void delete(Subjects groups) {
    getSession().delete(groups);
    return;
  }
  
  @SuppressWarnings("unchecked")
  public List<Subjects> getAll() {
    return getSession().createQuery("from Subjects").list();
  }
 
  public Subjects getById(int id) {
	  Subjects groups=null;
	  try
	  {
		  
		  Criteria crit = getSession().createCriteria(Subjects.class);
		  crit.add(Restrictions.eq("id",id));
		  groups=(Subjects) crit.uniqueResult();
	    return groups;
	  }catch(Exception e)
	  {
		  System.out.println(e.getMessage());
		  return groups;
	  }
  }
  @SuppressWarnings("unchecked")
public List<Subjects> getSubject(String username) {
	  List<Subjects> subject=null;
	  try
	  {
		  Criteria crit = getSession().createCriteria(Groups.class);
		  crit.add(Restrictions.eq("name",username));
		  subject=crit.list();
	    return subject;
	  
	  }catch(Exception e)
	  {
		  return subject;
	  }
	  }

  public void update(Subjects groups) {
    getSession().update(groups);
    return;
  }

} // class UserDao