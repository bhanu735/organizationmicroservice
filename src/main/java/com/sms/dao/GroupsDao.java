package com.sms.dao;

import java.util.List;
import javax.transaction.Transactional;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.sms.model.Groups;
import com.sms.model.Organizations;


@Repository
@Transactional
public class GroupsDao {
  
  @Autowired
  private SessionFactory _sessionFactory;
  
  private Session getSession() {
    return _sessionFactory.getCurrentSession();
  }

  public int save(Groups groups) {
    int id=(int) getSession().save(groups);
    return id;
  }
  
  public void delete(Groups groups) {
    getSession().delete(groups);
    return;
  }
  
  @SuppressWarnings("unchecked")
  public List<Groups> getAll() {
    return getSession().createQuery("from Groups").list();
  }
 
  public Groups getById(int id) {
	  Groups groups=null;
	  try
	  {
		  
		  Criteria crit = getSession().createCriteria(Groups.class);
		  crit.add(Restrictions.eq("id",id));
		  groups=(Groups) crit.uniqueResult();
	    return groups;
	  }catch(Exception e)
	  {
		  System.out.println(e.getMessage());
		  return groups;
	  }
  }
  @SuppressWarnings("unchecked")
public List<Groups> getByGroupName(String username) {
	  List<Groups> group=null;
	  try
	  {
		  Criteria crit = getSession().createCriteria(Groups.class);
		  crit.add(Restrictions.eq("name",username));
		  group=crit.list();
	    return group;
	  
	  }catch(Exception e)
	  {
		  return group;
	  }
	  }

  public void update(Groups groups) {
    getSession().update(groups);
    return;
  }

} // class UserDao