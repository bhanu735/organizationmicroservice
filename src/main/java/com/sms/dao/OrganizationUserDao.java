package com.sms.dao;

import java.math.BigInteger;
import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sms.model.OrganizationUsers;
import com.sms.model.Organizations;
import com.sms.model.User;

@Repository
@Transactional
public class OrganizationUserDao {
  
  @Autowired
  private SessionFactory _sessionFactory;
  
  private Session getSession() {
    return _sessionFactory.getCurrentSession();
  }

  public Integer save(OrganizationUsers org) {
    Integer id= (Integer) getSession().save(org);
    return id;
  }
  
  public void delete(OrganizationUsers org) {
    getSession().delete(org);
    return;
  }
  
  @SuppressWarnings("unchecked")
  public List<OrganizationUsers> getAll(int orgId) {
    return getSession().createQuery("from OrganizationUsers where orgId=:orgId")
    		.setParameter("orgId",orgId)
    		.list();
  }
 

@SuppressWarnings("unchecked")
public List<OrganizationUsers> getByEmail(String email,int orgId) {
	  List<OrganizationUsers> org=null;
	  try
	  {
		  Criteria crit = getSession().createCriteria(OrganizationUsers.class);
		  crit.add(Restrictions.eq("email",email));
		  crit.add(Restrictions.eq("orgId",orgId));
		  org= crit.list();
		  
		  return org;
	  }catch(Exception ex)
	  {
		  return org;
	  }
    
  }

  public OrganizationUsers getById(String email) {
	  OrganizationUsers org=null;
	  try
	  {
		  
		  Criteria crit = getSession().createCriteria(OrganizationUsers.class);
		  crit.add(Restrictions.eq("email",email));
		   org=(OrganizationUsers) crit.uniqueResult();
	    return org;
	  }catch(Exception e)
	  {
		  System.out.println(e.getMessage());
		  return org;
	  }
	 
  }
  public OrganizationUsers getByUserName(String user_name,String password,int orgId) {
	  OrganizationUsers org=null;
	  try{
	  return (OrganizationUsers) getSession().createQuery(
		        "from OrganizationUsers where email =:email and password=:password and orgId=:orgId" )
		        .setParameter("email", user_name)
		        .setParameter("password", password)
		        .setParameter("orgId", orgId)
		        .uniqueResult();
	  }catch (Exception e) {
		  //e.printStackTrace();
		  return org;
		
		// TODO: handle exception
	}

	  
	  
  }

  public void update(OrganizationUsers org) {
    getSession().update(org);
    return;
  }

} // class UserDao